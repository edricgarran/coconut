#!/usr/bin/env python3

import os
import sys
import argparse
import tempfile
import subprocess
import importlib

import sqlalchemy as sa
from alembic.runtime.migration import MigrationContext
from alembic.operations import Operations
from alembic import autogenerate


def upgrade(metadata, database_url, editor):
    engine = sa.create_engine(database_url)
    with engine.connect() as connection:
        context = MigrationContext.configure(connection)
        script = autogenerate.produce_migrations(context, metadata)
        with tempfile.NamedTemporaryFile(suffix=".migration.py", mode="w+") as temp:
            print("def run():", file=temp)
            print(autogenerate.render_python_code(script.upgrade_ops), file=temp)
            print("run()", file=temp, flush=True)
            subprocess.call([editor, temp.name])

            if input("Confirm execution? y/N ").lower() == "y":
                temp.seek(0)
                code = compile(temp.read(), "", mode="exec")
                with connection.begin():
                    # pylint: disable=exec-used
                    exec(code, {"sa": sa, "op": Operations(context)})


def main(args):
    parser = argparse.ArgumentParser(
        description=(
            "Statelessly migrate a database to match the current"
            " SQLAlchemy schema metadata."
        ),
    )
    parser.add_argument(
        "metadata", help="Python module containing the metadata definition.",
    )
    parser.add_argument(
        "--database-url",
        dest="database_url",
        default=os.environ.get("DATABASE_URL", "sqlite://"),
        help=(
            "Database URL in SQLAlchemy format."
            " Defaults to the DATABASE_URL environment variable, if available,"
            " or `sqlite://` otherwise."
        ),
    )
    parser.add_argument(
        "--editor",
        dest="editor",
        default=os.environ.get("EDITOR", "/bin/vi"),
        help=(
            "Editor to use to review the migration script."
            " Defaults to the EDITOR environment variable, if available,"
            " or `/bin/vi` otherwise."
        ),
    )
    args = parser.parse_args(args)
    sys.path.append(".")
    metadata = importlib.import_module(args.metadata).metadata
    upgrade(metadata, args.database_url, args.editor)


def cli():
    main(sys.argv[1:])
